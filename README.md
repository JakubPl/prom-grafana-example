# Micrometer & Prometheus & Grafana example

Playground for micrometer -> prometheus -> grafana bundle

## How to run

```
cd docker
docker-compose up
```
### Paths
| App                         | Path                                 | ...              |
|-----------------------------|--------------------------------------|------------------|
| Prometheus                  | `localhost:9090`                     |                  | 
| Grafana                     | `localhost:3000`                     | u:admin p:secret | 
| Simple app metrics endpoint | `localhost:9014/actuator/prometheus` |                  | 
| Simple app hello endpoint   | `localhost:9015/hello`               |                  | 
 

