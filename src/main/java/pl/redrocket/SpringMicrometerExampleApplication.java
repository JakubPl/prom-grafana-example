package pl.redrocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMicrometerExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMicrometerExampleApplication.class, args);
    }
}
