package pl.redrocket.configuration;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.system.UptimeMetrics;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
public class MetricConfiguration {
    private MeterRegistry meterRegistry;

    public MetricConfiguration(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void gauge() {
        Gauge.builder("app.info", () -> 1)
                .tag("version", "1.1.1")
                .tag("branch", "master")
                .register(meterRegistry);
    }

    @Bean
    public Counter myCounter() {
        return Counter.builder("myCounter")
                .baseUnit("s")
                .register(meterRegistry);
    }

    @Bean
    public JvmMemoryMetrics memory() {
        return new JvmMemoryMetrics();
    }

    @Bean
    public UptimeMetrics uptimeMetrics() {
        return new UptimeMetrics();
    }
}
