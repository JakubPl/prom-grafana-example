package pl.redrocket.controller;


import io.micrometer.core.instrument.Counter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SomeController {
    private Counter myCounter;

    public SomeController(Counter myCounter) {
        this.myCounter = myCounter;
    }

    @GetMapping("hello")
    public String get() {
        this.myCounter.increment();
        return "OK";
    }
}
